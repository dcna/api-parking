"""myapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from parking.views import (
    VehicleView,
    VehicleViewDetail,
    ParkingView,
    ParkingViewDetail,
    ParkedVehiclesView
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('vehicles', VehicleView.as_view(), name='list-vehicles'),
    path('vehicles/<int:pk>', VehicleViewDetail.as_view(), name='get-vehicle'),
    path('parking', ParkingView.as_view(), name='list-parkings'),
    path('parking/<int:pk>', ParkingViewDetail.as_view(), name='get-parking'),
    path('parked_vehicles', ParkedVehiclesView.as_view(), name='get-parking')

]

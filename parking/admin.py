from django.contrib import admin

from .models import Parking, Vehicle

# Register your models here.

admin.site.register(Vehicle)
admin.site.register(Parking)
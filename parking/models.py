from django.db import models

# Create your models here.

class Vehicle(models.Model):
    client_name = models.CharField(max_length=255, blank=False, null=False)
    car_model = models.CharField(max_length=255, blank=False, null=False)
    plate = models.CharField(max_length=10, blank=False, null=False)
    color = models.CharField(max_length=50, blank=False, null=False)
    photo = models.ImageField(null=True, blank=True)

    def __str__(self):
        return f'ID: {self.id} - {self.car_model}/{self.color} - \
                {self.plate} - {self.client_name}'


class Parking(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    entry_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    departure_date = models.DateTimeField(auto_now_add=False, auto_now=False, blank=True, null=True)

    def __str__(self):
        return f'Entrance: {self.entry_date.strftime("%d/%m/%Y %H:%M:%S")} - \
                {self.vehicle.car_model}/{self.vehicle.color} - {self.vehicle.plate} - \
                {self.vehicle.client_name}'
from dataclasses import fields
from rest_framework import serializers

from parking.models import Parking, Vehicle

class ParkingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parking
        fields = ("__all__")


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ("__all__")        
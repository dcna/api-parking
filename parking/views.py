from django.shortcuts import render
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, serializers, generics

from .models import Parking, Vehicle

from .api.serializers import ParkingSerializer, VehicleSerializer

# Create your views here.

class VehicleView(APIView):
    """
    List vehicles and creates new
    """

    def get(self, request, format=None):
        vehicle = Vehicle.objects.all()
        print(vehicle)
        serializer = VehicleSerializer(vehicle, many=True)
        print(serializer.data)
        return Response(serializer.data)

    
    def post(self, request, format=None):

        if Vehicle.objects.filter(**request.data).exists():
            raise serializers.ValidationError('Vehicle already exists')

        serializer = VehicleSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VehicleViewDetail(APIView):
    """
    Get vehicle details
    """

    def get_object(self, pk):
        try:
            return Vehicle.objects.get(pk=pk)
        except Vehicle.DoesNotExist:
            raise Http404
    

    def get(self, request, pk, format=None):
        vehicle = self.get_object(pk)
        serializer = VehicleSerializer(vehicle)
        return Response(serializer.data)


    def delete(self, request, pk, format=None):
        vehicle = self.get_object(pk)
        vehicle.delete()
        return Response(status.HTTP_204_NO_CONTENT)

    
    def put(self, request, pk, format=None):
        vehicle = self.get_object(pk)
        serializer = VehicleSerializer(vehicle, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ParkingView(generics.ListCreateAPIView):
    queryset = Parking.objects.all()
    serializer_class = ParkingSerializer

    def post(self, request, *args, **kwargs):

        vehicle = Vehicle.objects.get(id=request.data.get('vehicle'))
        parked_vehicle = Parking.objects.filter(vehicle=vehicle, departure_date__isnull=True)
        if parked_vehicle.exists():
            raise serializers.ValidationError('The vehicle has an open entrance')

        #return self.create(request, *args, **kwargs)
        return super().post(request, *args, **kwargs)


class ParkingViewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Parking.objects.all()
    serializer_class = ParkingSerializer


class ParkedVehiclesView(APIView):
    """
    List all parked vehicles
    """

    def get(self, request, format=None):
        parked_vehicles = Parking.objects.filter(
            departure_date__isnull=True
        ).values(
            'entry_date', 'departure_date',
            'vehicle__client_name', 'vehicle__car_model', 'vehicle__plate',
            'vehicle__color', 'vehicle__photo', 'vehicle__id'
        )
        return Response(parked_vehicles)
